//console.log("hello");

let address = "https://jsonplaceholder.typicode.com/todos";

fetch(address)
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});

//---------------------------

fetch(address + "/1")
.then(res => res.json())
.then(response => {
	console.log(response);
	console.log("The item '" + response.title + "' on the list has a status of " + response.completed);
});

//---------------------------

fetch(address, {
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			completed: false,
			title: "Created To Do List Item",
			userId: 1
		})
})
.then(response => response.json())
.then(json => console.log(json))

//---------------------------

fetch(address + "/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


/* ayaw gumana... nauna pa sa output ang status complete bago status pending haha
async function fetchData(){
	let result = await(fetch(address + "/1", {
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	})
	);
	let json = await result.json();
	console.log(json);
}
fetchData();
*/

//---------------------------

fetch(address + "/1", {
	method: "PATCH",
	headers: {
			"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "02/08/23",
		status: "Complete"
	})
})
.then(response => response.json())
.then(json => console.log(json))

//---------------------------

fetch(address + "/2", {
	method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json))
